#!/usr/bin/env perl

use Test::More tests => 9;

use IO::File;

use strict;
use warnings;

my ($tmpdir, $tmpconf);
BEGIN {
    $tmpdir = "/tmp/spamcat.t.$$";
    $tmpconf = "$tmpdir/spamcat.conf";

    system("rm -rf $tmpdir") == 0
        or die "Couldn't remove $tmpdir: $!\n";
    mkdir $tmpdir;
    system("sqlite3 $tmpdir/spamcat.sqlite3 < config/create-tables.sql") == 0
        or die "Couldn't create $tmpdir/spamcat.sqlite3: $!\n";

    my $fh = IO::File->new("> $tmpdir/spamcat.conf") ||
        die "Couldn't create $tmpdir/spamcat.conf: $!\n";
    print $fh "DBPATH = $tmpdir/spamcat.sqlite3\n";
    print $fh "DEFAULT_COUNT = 20\n";
    print $fh "DOMAINS = spamcat.example.com\n";
}

END {
    system "rm -rf $tmpdir";
}

my $spamcat  = 'bin/spamcat';
my $conffile = 't/fixtures/sample.conf';

# Add testlib which has createdb and possibly population of said db.

my @dumpconfig = `$spamcat -c $conffile dumpconfig`;
my %got = parse_configdump(@dumpconfig);
my %expected = (DBPATH	      => '/tmp/spamcat.sqlite3',
		DEFAULT_COUNT => 10,
                DOMAINS	      => "spamcat.example.com, spamcat2.example.com, spamcat3");
is_deeply(\%got, \%expected);

# Unknown senders have the default number of mails left.
my @got = `$spamcat -c $tmpconf get unknown-sender`;
is_deeply(\@got, ["unknown-sender has 20 messages remaining.\n"]);

# Missing argument to set fails.
my $got = system("$spamcat -c $tmpconf set missing-argument") >> 8;
is($got, 1);

# Can set/get remaining count.
@got = `$spamcat -c $tmpconf set foo 1`;
is($?, 0);
is_deeply(\@got, ["foo has 1 messages remaining.\n"]);
@got = `$spamcat -c $tmpconf get foo`;
is_deeply(\@got, ["foo has 1 messages remaining.\n"]);

@got = `$spamcat -c $tmpconf set foo 0`;
is($?, 0);
is_deeply(\@got, ["foo has 0 messages remaining.\n"]);
@got = `$spamcat -c $tmpconf get foo`;
is_deeply(\@got, ["foo has 0 messages remaining.\n"]);

sub parse_configdump {
  my %rc;

  while (my $line = shift) {
    $line =~ /(.*) = (.*)/;
    $rc{$1} = $2;
  }

  %rc;
}
